# SampleNFT
## Requirements
### Contract
* Solidity version 0.8.x
* OpenZeppelin contracts version 4.x
### Python script
* Python version 3.6+
* Libraries: eth_account, w3 
## Features 
* Standard ERC721 token
* Payment Splitter - definable balance withdraw address / adresses - shares
* Token limit - MAX_TOKENS
* Definable price tiers / bundles
* Token presale / sale start / sale stop
* Presale whitelisting based on address (Python scrypt) or specific token holdings (ExternalContract)
* magicMint - Contract owner is allowed to mint for free
## Usage
### Variables
FIXED:
* MAX_TOKENS - Token amount limit
* MIN_PRICE - Minimum price multiplier - in wei

EDITABLE:
* baseURI - Token storage address prefix (must end in '/') - set at contract creation
* PRICING - Price table - 
DEFAULT [[1,100],[5,85],[10,75]]

|Tokens bought| Single token price|
|--|--|
|1-4|0.01 ETH|
|5-9|0.085 ETH|
|10 or more|0.075ETH|

* hasSaleStarted - BOOLEAN - default FALSE
* whiteLister - Address of Whitelister - Used for signing addresses of whitelisted minters (Python scrypt) 
* ownNFTwhitelist - Placeholder for contract address of past tokens ( can be ERC20 or ERC721) - required to hold for presale interaction - mintOwnNFT

### Functions
* setBaseURI
* startSale
* pauseSale
* setWhitelister - set Whitelister address, default is 0x000...
* setOwnNFTwhitelist - set address of past contract used for presale interaction - mintOwnNFT
* setupPricing - used for updating the pricing table
* mint - classic token mint function, usable once the sale has started
* mintWhiteListed - presale token mint function used with signature hash - address of presale buyer needs to be signed by 'whiteLister' - (Python scrypt) 
* mintOwnNFT - presale mint function used by buyers that own a specific token granting them access - ownNFTwhitelist
* magicMint - contract owner mint function - free to execute

### Deployment
![deployment](https://i.ibb.co/ckGr6Vr/constructor.jpg)
* NAME
* SYMBOL
* BASE_URI
* PaymentSplitter requires payees and shares to be submitted in an array:\
EXAMPLE: \
PAYEES = [addr1,addr2,addr3] SHARES [90,9,1]\
Funds we be withdrawable 90% to addr1 9% to addr2 and 1% to addr3\
PAYEES = [addr1,addr2] SHARES [1,1]\
Funds we be withdrawable 50% to addr1 and 50% to addr2


Payees and shares are defined only at deployment and cannot be modified.

---
## NFT Sale Timeline with _sample data_
### 1. Contract Deployement

* NAME:
string - _PumpKingzNFT_
* SYMBOL:
string - _PKING_
* BASE_URI:
string - _https://ipfs.io/ipfs/PumpKingzNFT/_
* PAYEES:
address[] - _[0xFfDFbcBfc639E149dc202d6642EfE3E1Aa21ed7D]_
* SHARES:
uint256[] - _[1]_

### 2. Setting up presale details
* function setWhitelister - address\
Sets address of whitelister - PrivateKey to be then used in Python scrypt for signing whitelisted addresses\
_setWhitelister(0xFfDFbcBfc639E149dc202d6642EfE3E1Aa21ed7D)_
* function setOwnNFTwhitelist - address \
Sets address of past (NFT) contract - to enable presale minting for holders of specific (NFT) tokens.\
_setOwnNFTwhitelist(0x7AB2352b1D2e185560494D5e577F9D3c238b78C5)_

***
### 3. Presale start
Users can start minting utilizing two mint methods, numberical parameter is the number of tokens being bought:
* function mintWhiteListed - uint8, bytes\
Users should have whitelisted address - signed address signature recived from Python scrypt\
_mintWhiteListed(1, 0xf4483234605946446bc950d4f0495d6fbab4e6ad44dfdce806308eb921ddd5f050e21231f1f0d6fadf575ece004828a7e6ab40858f97f4e9f7ac2e548dbf940d1c)_
* function mintOwnNFT - uint8 \
Users should hold token of past contract / NFT sale defined in setOwnNFTwhitelist\
_mintOwnNFT(1)_
***
### 4. Sale start
Possible utilization of setupPricing to change token prices
* function startSale - boolean\
_startSale(true) / startSale(1)_
* function mint - uint8\
Classic mint function, with no requirements\
_mint(1)_

### 5. "Cashing out"
* function release - address\
Sends Contract funds to address / addresses set at contract deployment\
_release(0xFfDFbcBfc639E149dc202d6642EfE3E1Aa21ed7D)_
***
### Optional to do anytime
* function transferOwnership - address\
Sets contract ownership to new address
_transferOwnership(0xFfDFbcBfc639E149dc202d6642EfE3E1Aa21ed7D)_
* function pauseSale\
stops exectution of mint function, can be reset by function startSale\
_pauseSale()_
* function setupPricing - uint8[2][]\
Sets new pricing rules\
_setupPricing([[1,100]])_
* function setBaseURI - string\
changes the NFT storage location\
_setBaseURI("https://buy-my-nft.com/tokens/")_